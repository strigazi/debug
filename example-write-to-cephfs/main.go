package main

import (
    "fmt"
    //"io/ioutil"
    "os"
    "time"
)

func check(e error) {
    if e != nil {
        panic(e)
    }
}

func somewrite() {
	//f, err := os.Create("/tmp/dat1")
	f, err := os.OpenFile("/tmp/dat1", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	check(err)
	l, err := f.WriteString("Hello\n")
	check(err)
	fmt.Println(l, "bytes written successfully")
	err = f.Close()
	check(err)
	//d1 := []byte("hello\n")
	//err := ioutil.WriteFile("/opt/example/dat1", d1, 0644)
}

func main() {

    for j := 7; j <= 3600; j++ {
	somewrite()
        fmt.Println("Wrote hello, now sleeping 1s")
        time.Sleep(1 * time.Second)
    }

}

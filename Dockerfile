FROM debian:bullseye-slim

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
	apt-get install -y --no-install-recommends \
	dnsutils \
	procps \
	iputils* \
	telnet \
	smem \
	traceroute \
	procps \
	htop \
	curl \
	strace \
	sysstat \
	iperf \
	iproute2 \
	iotop \
	pciutils \
	tree \
	bash-completion \
	bridge-utils \
	qperf \
	jq \
	screen \
	tmux \
	systemd \
	less \
	fio \
	man \
	tcpdump
